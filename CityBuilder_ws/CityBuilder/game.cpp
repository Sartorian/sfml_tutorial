#include <stack>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "game.hpp"
#include "game_state.hpp"

namespace CityBuilder
{
	
//Adds a state to the stack
void Game::pushState(State::Base* state)
{
	this->states.push(state);
}

//Removes a state from the stack and frees the memory
void Game::popState()
{
	delete this->states.top();
	this->states.pop();
}

//Pops the current state and pushes a new one.
void Game::changeState(State::Base* state)
{
	if(!this->states.empty())
	{
		popState();
	}
	pushState(state);
}

//Returns the current state.
State::Base* Game::peekState()
{
	if(this->states.empty()) 
	{
		return nullptr;
	}
	return this->states.top();
}

//The core loop of the game
void Game::gameLoop()
{
	sf::Clock clock;
	
	while(this->window.isOpen())
	{
		sf::Time elapsed = clock.restart();
		float dt = elapsed.asSeconds();
		if(peekState() == nullptr) continue;
		peekState()->handleInput();
		peekState()->update(dt);
		this->window.clear(sf::Color::Black);
		peekState()->draw(dt);
		this->window.display();
	}
}

//Creates the window
Game::Game()
{
	this->window.create(sf::VideoMode(800,600), "City Builder");
	this->window.setFramerateLimit(60);
}

//Game destructor, cleans up all remaining memory.
Game::~Game()
{
	//Delete all the states before ending the process.
	while(!this->states.empty()) popState();
}

}//END NAMESPACE