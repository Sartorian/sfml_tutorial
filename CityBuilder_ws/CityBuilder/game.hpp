#ifndef GAME_HPP
#define GAME_HPP

#include <stack>
#include <SFML/Graphics.hpp>

namespace CityBuilder
{
	//Namespace containing all game states
	namespace State
	{
		class Base;
	}//END NAMESPACE STATE

	//Core GAME class
	class Game
	{
	public:
		std::stack<State::Base*> states;
	
		sf::RenderWindow window;
	
		void pushState(State::Base* state);
		void popState();
		void changeState(State::Base* state);
		State::Base* peekState();
	
		void gameLoop();
	
		Game();
		virtual ~Game();
	};//END GAME CLASS
}//END NAMESPACE CITYBUILDER
#endif // GAME_HPP
