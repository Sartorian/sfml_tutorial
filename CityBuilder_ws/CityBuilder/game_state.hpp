#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include "game.hpp"

namespace CityBuilder
{
	namespace State
	{
		class Base
		{
		public:
	
			Base();
			virtual ~Base();
	
			Game* game;
			
			virtual void draw(const float dt) = 0;
			virtual void update(const float dt) = 0;
			virtual void handleInput() = 0;
		};
	}//END NAMESPACE STATE
}//END NAMESPACE CITYBUILDER
#endif // GAME_STATE_HPP
